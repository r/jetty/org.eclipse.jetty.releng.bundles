#!/bin/sh
#Move the generated p2 repository to its location on download.eclipse.org/jetty/updates
pwd

#export pack_and_sign
#export jetty_release_version
#export force_context_qualifier

deploymentdir="/home/data/httpd/download.eclipse.org/jetty/updates/jetty-bundles-9.x"
[ -z "$deploymentdir_name" ] && deploymentdir_name="jetty-bundles-9.x"
builtrepodir="jetty.bundles.repo/target/site"
if [ ! -d "$deploymentdir" ]; then
  echo "Unable to find the deployment directory $deploymentdir"
  exit 12
fi
if [ ! -d "$builtrepodir/plugins/" ]; then
  echo "Unable to find the built repository directory $builtrepodir"
  exit 12
fi
jetty_util=`find $builtrepodir/plugins/org.eclipse.jetty.util_*.jar`
if [ -z "$jetty_util" ]; then
  echo "Unable to locate 'org.eclipse.jetty.util_*.jar' the repository was not built correctly."
  exit 12;
fi
jetty_version_built=`echo "$jetty_util" | sed -nr 's/.*util_(.*)(\.jar)/\1/p'`
echo "version built $jetty_version_built"

jetty_version_used=$jetty_version_built
if [ -z "$jetty_release_version" ]; then
  echo "SNAPSHOT build: deploy in the 'development' folder."
  jetty_version_built="development"
fi

echo "Place the product archives inside the repository"
repository_archive_file_name="Jetty-bundles-repository-"$jetty_version_used".zip"
[ -d "jetty.bundles.repo/target/$repository_archive_file_name" ] && rm "jetty-features/org.eclipse.jetty.product/target/$repository_archive_file_name"
zip -r "jetty.bundles.repo/target/$repository_archive_file_name" jetty.bundles.repo/target/site

mv jetty.bundles.repo/target/$repository_archive_file_name $builtrepodir/

if [ -d "$deploymentdir/$jetty_version_built" ]; then
  rm -rf $deploymentdir/$jetty_version_built
fi
mkdir -p $deploymentdir/$jetty_version_built

echo "Generating the index.html"
#For the synthax of the html template see http://stackoverflow.com/questions/1609423/using-sed-to-expand-environment-variables-inside-files

deploymentdir_absolute_href="/jetty/updates/$deploymentdir_name/$jetty_version_built"
software_site_url=http://download.eclipse.org${deploymentdir_absolute_href}
repository_archive_file_href="http://www.eclipse.org/downloads/download.php?file=$deploymentdir_absolute_href/$repository_archive_file_name"
timestamp=`date`
echo $timestamp
export software_site_url deploymentdir_absolute_href repository_archive_file_name repository_archive_file_href jetty_version_used timestamp
env | sed 's/[\%]/\\&/g;s/\([^=]*\)=\(.*\)/s%${\1}%\2%/;/^s/!d' > sed.script
cat index-tpl.html | sed -f sed.script > $builtrepodir/index.html


if [ -d "$deploymentdir/$jetty_version_built" ]; then
  rm -rf $deploymentdir/$jetty_version_built
fi
mkdir -p $deploymentdir/$jetty_version_built

echo "Deploying the generated p2 repository in $deploymentdir/$jetty_version_built"
cp -r $builtrepodir/* $deploymentdir/$jetty_version_built
#Make sure that the rt.jetty group of users can tweak things eventually. Ignore errors.
set +e
chmod -R g+rw $deploymentdir/$jetty_version_built
set -e

#Re-generate the composite repository
[ -f "$deploymentdir/index.sh" ] && $deploymentdir/index.sh

